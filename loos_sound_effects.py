#! /usr/bin/env python3
# loos_sound_effects.py:
#                Wimborne Model Town
#      Loos MP3 Player Software
#
#  **********************************************************************
# This program carries out the following functions:
#  1.  Plays a recording of the toilets flushing in the public loos.
#  2.  Pauses for a time (4.5 minutes).
#  3.  Plays a recording of the phone ringing in the adjacent Call Box.
#  4.  Pauses for  a time (4.5 minutes).
#  5.  Repeats steps 1 to 4 continuously.
#  6.  Lights an orange LED as soon as the sequence starts and then:
#        Continuously ON while the recordings are played.
#        Flashing at 1 second ON and one second OFF during pauses.
#
# See the associated documentation for the circuit diagram, Installation
# Specification and other information.
#
# Copyright (c) 2019 Wimborne Model Town http://www.wimborne-modeltown.com/
#
#    This code is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Scheduler.py is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    with this software.  If not, see <http://www.gnu.org/licenses/>.
#
#  ***********************************************************************

import time
import os
import subprocess
import RPi.GPIO as GPIO

import logging
logging.basicConfig()

################################### Setup GPIO  ######################################

GPIO.setmode(GPIO.BCM)

GPIO.setup(17, GPIO.OUT)                    # orange LED

################## Setup Variables - Use globals to make configuration easy.  ############################

# General vars.
orange_led = 17                             # GPIO pin for the orange LED.
pause = 4.5                                 # Time in minutes between each sound effect.

# MP3 Functions
# Locations for the different MP3 files.
loo_flushing = '/home/pi/Loos_Sound_Effects/Sounds/loo_flushing.mp3'
phone_ringing = '/home/pi/Loos_Sound_Effects/Sounds/phone_ringing.mp3'


################################################  Calculate Delays  #######################################

delay = pause * 60                      # Time in seconds between each sound effect.

################################################ Define Procedures  #######################################

def play_loos_flush():                      #  Setup and run the MP3 Player
    global loo_flushing

    print ('Flushing the Loos')

    subprocess.call(['mpg321', loo_flushing])

def play_phone_ringing():                   #  Setup and run the MP3 Player
    global loo_flushing

    print ('Ringing the Phone')

    subprocess.call(['mpg321', phone_ringing])

def orange_led_on():
    GPIO.output(orange_led, GPIO.HIGH)       # LED on

def orange_led_off():
    GPIO.output(orange_led, GPIO.LOW)        # LED off

    

################################################ Main Program Begins Here  ###########################################

try:
    while True:
        orange_led_on()
        play_loos_flush()
        
        i = delay/2                         # Initialise loop count to the number of seconds to count down from Pause in 2 second steps 

        print("Starting loop to generate a delay of half of required time.")
        
        while i > 0:                        # Loop runs for half of pause minutes
            orange_led_on()
            time.sleep(1)
            orange_led_off()
            time.sleep(1)
            i -= 1

        orange_led_on()
        play_phone_ringing()
        
        i = delay/2                         # Initialise loop count
        
        print("Starting loop to generate a delay of half of required time.")
        
        while i > 0:                        # Loop runs for half of pause minutes
            orange_led_on()
            time.sleep(1)
            orange_led_off()
            time.sleep(1)
            i -= 1


except KeyboardInterrupt:
    GPIO.cleanup()
    print("")
    print("Please Wait...")
    time.sleep(5)   

    
